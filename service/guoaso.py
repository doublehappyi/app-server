# coding:utf8
from base import ServiceBase
from model.guoaso import ModelGuoaso
from tornado.gen import coroutine, Return
from utils import time as utils_time


class ServiceGuoaso(ServiceBase):
  def __init__(self, *args, **kwargs):
    super(ServiceGuoaso, self).__init__(*args, **kwargs)
    self.model_guoaso = ModelGuoaso()

  @coroutine
  def find_all(self):
    res = yield self.model_guoaso.find_all()
    raise Return(res)

  @coroutine
  def find(self, author,type):
    day_tomorrow = utils_time.days_ago(-1)
    day_today = utils_time.days_ago(0)
    day_yesterday = utils_time.days_ago(1)
    # days_ago_7 = utils_time.days_ago(7)
    # days_ago_8 = utils_time.days_ago(8)
    # days_ago_30 = utils_time.days_ago(30)
    # days_ago_31 = utils_time.days_ago(31)

    # 今天的数据
    today = yield self.model_guoaso.find(day_today, day_tomorrow,type, author)
    # 昨天的数据
    yesterday = yield self.model_guoaso.find(day_yesterday, day_today,type, author)
    # 一周前的数据
    # week = yield self.model_guoaso.find(days_ago_8, days_ago_7)
    # 一个月前的数据
    # month = yield self.model_guoaso.find(days_ago_31, days_ago_30)

    dict_today = {}
    dict_yesterday = {}
    # dict_week = {}
    # dict_month = {}

    for item in today:
      dict_today[item.appid] = item

    for item in yesterday:
      dict_yesterday[item.appid] = item

    # for item in week:
    #   dict_week[item.appid] = item
    #
    # for item in month:
    #   month[item.appid] = item

    for item in today:
      appid = item['appid']
      item['yesterday'] = {
        "cover_all_delta": '-',
        "cover_top3_delta": '-',
        "cover_top10_delta": '-'
      }
      # item['week'] = {
      #   "cover_all_delta": '-',
      #   "cover_top3_delta": '-',
      #   "cover_top10_delta": '-'
      # }
      # item['month'] = {
      #   "cover_all_delta": '-',
      #   "cover_top3_delta": '-',
      #   "cover_top10_delta": '-'
      # }

      # dict_week_curr = dict_week[appid] if appid in dict_week else None
      dict_yesterday_curr = dict_yesterday[appid] if appid in dict_yesterday else None
      # dict_month_curr = dict_month[appid] if appid in dict_month else None

      if dict_yesterday_curr:
        item['yesterday']['cover_all_delta'] = item['cover_all'] - dict_yesterday_curr['cover_all']
        item['yesterday']['cover_top3_delta'] = item['cover_top3'] - dict_yesterday_curr['cover_top3']
        item['yesterday']['cover_top10_delta'] = item['cover_top10'] - dict_yesterday_curr['cover_top10']

      # if dict_week_curr:
      #   item['week']['cover_all_delta'] = item['cover_all'] - dict_week_curr['cover_all']
      #   item['week']['cover_top3_delta'] = item['cover_top3'] - dict_week_curr['cover_top3']
      #   item['week']['cover_top10_delta'] = item['cover_top10'] - dict_week_curr['cover_top10']
      #
      # if dict_month_curr:
      #   item['month']['cover_all_delta'] = item['cover_all'] - dict_month_curr['cover_all']
      #   item['month']['cover_top3_delta'] = item['cover_top3'] - dict_month_curr['cover_top3']
      #   item['month']['cover_top10_delta'] = item['cover_top10'] - dict_month_curr['cover_top10']

    raise Return(today)

  @coroutine
  def find_authors_today(self):
    data = yield self.model_guoaso.find_authors_today()
    raise Return(data)

  @coroutine
  def find_keyword(self):
    data = yield self.model_guoaso.find_keyword()
    raise Return(data)

  @coroutine
  def order_keyword(self):
    data = yield self.model_guoaso.order_keyword()
    raise Return(data)

  @coroutine
  def delete_keyword_by_id(self, _id):
    data = yield self.model_guoaso.delete_keyword_by_id(_id)
    raise Return(data)

  @coroutine
  def delete_keyword_all(self):
    data = yield self.model_guoaso.delete_keyword_all()
    raise Return(data)

  @coroutine
  def add_sanmao_download_statistic(self):
    data = yield self.model_guoaso.add_sanmao_download_statistic()
    raise Return(data)

  @coroutine
  def add_sanmao_page_statistic(self):
    data = yield self.model_guoaso.add_sanmao_page_statistic()
    raise Return(data)

  @coroutine
  def get_sanmao_statistic(self):
    data = yield self.model_guoaso.get_sanmao_statistic()
    raise Return(data)
  @coroutine
  def get_sanmao_setting(self,result):
    data = yield self.model_guoaso.get_sanmao_setting(result)
    raise Return(data)
