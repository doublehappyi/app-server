# coding=utf8
from handler import index, guoaso, guoaso_api

router = [
    (r'/', guoaso.Guoaso),
    (r'/api/guoaso', guoaso_api.Guoaso),
    (r'/api/guoaso/update', guoaso_api.Guoaso),
    (r'/guoaso', guoaso.Guoaso),
    (r'/keyword', guoaso.Keyword),
    (r'/api/keyword/order', guoaso_api.KeywordOrder),
    (r'/api/keyword/(\d+)', guoaso_api.KeywordDelete),
    (r'/api/keyword/search', guoaso_api.KeywordSearch),
    (r'/keyword1', guoaso.KeywordOrder),
    (r'/api/keyword/delete/all', guoaso_api.KeywordDeleteAll),
    (r'/sanmao/app', guoaso.SanmaoApp),
    (r'/sanmao/statistic/download/page', guoaso.SanmaoStatisticDownloadPage),
    (r'/sanmao/statistic/download/click', guoaso.SanmaoStatisticDownloadClick),
    (r'/sanmao/statistic', guoaso.SanmaoStatistic),
    (r'/sanmao/setting', guoaso.SanmaoSetting)
]
