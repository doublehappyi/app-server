# coding:utf8
from base import ModelBase
from tornado.gen import coroutine, Return
import utils


class ModelGuoaso(ModelBase):
    @coroutine
    def find_all(self):
        sql = 'select * from cover'
        res = yield self.query(sql)
        raise Return(res)

    @coroutine
    def find(self, day_before, day_after,type, author=None):

        print "type is"
        print type
        if type in ['update_time', 'cover_all', 'cover_top3', 'cover_top10']:
            print "type is1111"
            print type
            if author:
                sql = 'select * from cover where updated_at > %s and updated_at < %s and author=%s '
                res = yield self.query(sql, day_before, day_after, author)
            else:
                sql = 'select * from cover where updated_at > %s and updated_at < %s '
                res = yield self.query(sql, day_before, day_after)
            raise Return(res)
        else:
            print "type is2222"
            print type
            if author:
                sql = 'select * from cover where updated_at > %s and updated_at < %s and author=%s and appname like %s'
                res = yield self.query(sql, day_before, day_after, author, type)
            else:
                sql = 'select * from cover where updated_at > %s and updated_at < %s and appname like %s'
                res = yield self.query(sql, day_before, day_after, type)
            raise Return(res)

    @coroutine
    def find_authors_today(self):
        today = utils.time.today()
        sql = '''select distinct author from cover where author!='' and  updated_at > %s''';
        res = yield self.query(sql, today)
        raise Return(res)

    @coroutine
    def find_keyword(self):
        sql = 'select * from keyword order by name desc'
        res = yield self.query(sql)
        raise Return(res)

    @coroutine
    def order_keyword(self):
        sql = 'select * from keyword order by result asc'
        res = yield self.query(sql)
        raise Return(res)
    @coroutine

    def delete_keyword_by_id(self, _id):
        sql = 'delete from keyword where id=%s'
        data = yield self.execute(sql, _id)
        raise Return(data)

    @coroutine
    def delete_keyword_all(self):
        sql = 'delete from keyword'
        data = yield self.execute(sql)
        raise Return(data)

    @coroutine
    def add_sanmao_download_statistic(self):
        data = yield self.get('select count from sanmao_statistic where type=2')

        sql = 'update sanmao_statistic as t set t.count = %s where type=2' % (data.count + 1)
        data = yield self.execute(sql)
        raise Return(data)

    @coroutine
    def add_sanmao_page_statistic(self):
        data = yield self.get('select count from sanmao_statistic where type=1')
        sql = 'update sanmao_statistic as t set t.count = %s where type=1' % (data.count + 1)
        data = yield self.execute(sql)
        raise Return(data)

    @coroutine
    def get_sanmao_statistic(self):
        data = yield self.query('select * from sanmao_statistic')
        raise Return(data)

    @coroutine
    def get_sanmao_setting(self,result):
        print  result
        path = '/Users/thomasyi/%s' % result
        file_object = open('/Users/thomasyi/11')
        try:
            all_the_text = file_object.read()
            all_the_text = all_the_text.replace("\"", "'");
            # all_the_text = all_the_text.replace(",", ",\n");
            sql = 'update sanmao_statistic set flag = "%s" where type = 1;' % all_the_text
            yield self.execute(sql)
            # logger.warning('Spider test !')
        finally:
            file_object.close()
            all_the_text = yield self.query('select * from sanmao_statistic')
        raise Return(all_the_text)
