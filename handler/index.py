from base import HandlerBase
from tornado.gen import coroutine, Return

class Index(HandlerBase):
  @coroutine
  def get(self, *args, **kwargs):
    self.write('hello index page')
