# coding:utf8
from base import HandlerBase
from tornado.gen import coroutine, Return
from service.guoaso import ServiceGuoaso


class Guoaso(HandlerBase):
    def __init__(self, *args, **kwargs):
        super(Guoaso, self).__init__(*args, **kwargs)
        self.srv_guoaso = ServiceGuoaso()

    @coroutine
    def get(self, *args, **kwargs):
        sortby = self.get_argument('sortby', 'cover_top10')
        author = self.get_argument('author', None)
        # 如果author=="all",则将author重置为None
        author = None if author == 'all' else author
        type = sortby;
        type = type.replace("|", "%")
        if sortby not in ['udpate_time', 'cover_all', 'cover_top3', 'cover_top10']:
            sortby = 'update_time'
        apps = yield self.srv_guoaso.find(author,type)
        authors = yield self.srv_guoaso.find_authors_today()
        # 对today数据排序
        apps.sort(key=lambda obj: obj.get(sortby), reverse=True)

        self.render('guoaso.html', apps=apps, authors=authors, filter_author=author, filter_sortby=sortby)


class Keyword(HandlerBase):
    def __init__(self, *args, **kwargs):
        super(Keyword, self).__init__(*args, **kwargs)
        self.srv_guoaso = ServiceGuoaso()

    @coroutine
    def get(self, *args, **kwargs):
        keyword_list = yield self.srv_guoaso.find_keyword()

        # keywords = ''
        # for item in keyword_list:
        #   keywords += item.keyword + ','
        self.render('keyword.html', keyword_list=keyword_list)

class KeywordOrder(HandlerBase):
    def __init__(self, *args, **kwargs):
        super(KeywordOrder, self).__init__(*args, **kwargs)
        self.srv_guoaso = ServiceGuoaso()

    @coroutine
    def get(self, *args, **kwargs):
        print 'xxx'
        keyword_list = yield self.srv_guoaso.order_keyword()

        # keywords = ''
        # for item in keyword_list:
        #   keywords += item.keyword + ','
        self.render('keyword.html', keyword_list=keyword_list)
class SanmaoApp(HandlerBase):
    def __init__(self, *args, **kwargs):
        super(SanmaoApp, self).__init__(*args, **kwargs)
        self.srv_guoaso = ServiceGuoaso()

    @coroutine
    def get(self, *args, **kwargs):
        self.render('video.html')

class SanmaoSetting(HandlerBase):
    def __init__(self, *args, **kwargs):
        super(SanmaoSetting, self).__init__(*args, **kwargs)
        self.srv_guoaso = ServiceGuoaso()

    @coroutine
    def get(self, *args, **kwargs):
        result = self.get_argument('result')

        data = yield self.srv_guoaso.get_sanmao_setting(result)
        self.render('sanmao-statistic.html', data=data)

class SanmaoStatistic(HandlerBase):
    def __init__(self, *args, **kwargs):
        super(SanmaoStatistic, self).__init__(*args, **kwargs)
        self.srv_guoaso = ServiceGuoaso()

    @coroutine
    def get(self, *args, **kwargs):
        data = yield self.srv_guoaso.get_sanmao_statistic()
        self.render('sanmao-statistic.html', data=data)


class SanmaoStatisticDownloadClick(HandlerBase):
    def __init__(self, *args, **kwargs):
        super(SanmaoStatisticDownloadClick, self).__init__(*args, **kwargs)
        self.srv_guoaso = ServiceGuoaso()

    @coroutine
    def get(self):
        data = yield self.srv_guoaso.add_sanmao_download_statistic()
        self.json_ok(data)


class SanmaoStatisticDownloadPage(HandlerBase):
    def __init__(self, *args, **kwargs):
        super(SanmaoStatisticDownloadPage, self).__init__(*args, **kwargs)
        self.srv_guoaso = ServiceGuoaso()

    @coroutine
    def get(self):
        data = yield self.srv_guoaso.add_sanmao_page_statistic()
        self.json_ok(data)
