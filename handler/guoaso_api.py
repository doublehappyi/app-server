# coding:utf8
from base import HandlerBase
from tornado.gen import coroutine, Return
from service.guoaso import ServiceGuoaso
import os


class Guoaso(HandlerBase):
  def __init__(self, *args, **kwargs):
    super(Guoaso, self).__init__(*args, **kwargs)
    self.srv_guoaso = ServiceGuoaso()

  @coroutine
  def get(self, *args, **kwargs):
    res = yield self.srv_guoaso.find()
    self.json_ok(res)

  @coroutine
  def post(self, *args, **kwargs):
    self.json_ok()

  def is_spider_running(self):
    cmd = "ps -ef|grep python|grep server|awk '{printf $2}'"

class KeywordOrder(HandlerBase):
  def __init__(self, *args, **kwargs):
    super(KeywordOrder, self).__init__(*args, **kwargs)
    self.srv_guoaso = ServiceGuoaso()

  @coroutine
  def post(self, *args, **kwargs):
    data = yield self.srv_guoaso.order_keyword()
    self.json_ok(data)

class KeywordDelete(HandlerBase):
  def __init__(self, *args, **kwargs):
    super(KeywordDelete, self).__init__(*args, **kwargs)
    self.srv_guoaso = ServiceGuoaso()

  @coroutine
  def post(self, _id, *args, **kwargs):
    data = yield self.srv_guoaso.delete_keyword_by_id(_id)
    self.json_ok(data)


class KeywordSearch(HandlerBase):
  def __init__(self, *args, **kwargs):
    super(KeywordSearch, self).__init__(*args, **kwargs)
    self.srv_guoaso = ServiceGuoaso()

  @coroutine
  def post(self, *args, **kwargs):
    url, index, result = self.get_argument('url'), self.get_argument('index'), self.get_argument('result')
    print '==========', index, result
    nweUrl = 'http://guoaso.com/keyword?appid="{0}"&country=cn&device=iphone'.format(url)
    command = 'python /Users/thomasyi/work/web/app-spider/guoaso3/main.py "{0}" {1} {2}'.format(nweUrl, index, result)
    print '==========', command
    os.system(command)
    # os.system('/usr/local/env/bin/python /home/yi/Git/app-spider/guoaso3/main.py %s %s %s' % (nweUrl, index, result))
    self.json_ok()


class KeywordDeleteAll(HandlerBase):
  def __init__(self, *args, **kwargs):
    super(KeywordDeleteAll, self).__init__(*args, **kwargs)
    self.srv_guoaso = ServiceGuoaso()

  @coroutine
  def post(self):
    data = yield self.srv_guoaso.delete_keyword_all()
    self.json_ok(data)
