import datetime


def now():
  return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def today():
  return days_ago(0)


def days_ago(days=0):
  return (datetime.date.today() - datetime.timedelta(days=days)).strftime('%Y-%m-%d') + " 00:00:00"
