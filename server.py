from tornado.ioloop import IOLoop
from tornado.web import Application
from config.site import config_site
from router import router

if __name__ == '__main__':
  app = Application(router, **config_site)
  port = 18888
  app.listen(port)
  print 'serving on http://localhost:' + str(port)
  IOLoop.instance().start()
